ARG BASE_CONTAINER=amd64/debian:buster-slim
FROM $BASE_CONTAINER

LABEL maintainer="Arjun Neupane <arjun@neupane.de" 

ARG NB_USER="ldv"
ARG NB_UID="1000"
ARG NB_GID="1000"

USER root

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -yq dist-upgrade && \
    echo 'path-exclude /usr/share/doc/*\npath-include /usr/share/doc/*/copyright\npath-exclude /usr/share/man/*\npath-exclude /usr/share/groff/*\npath-exclude /usr/share/info/*\npath-exclude /usr/share/lintian/*\npath-exclude /usr/share/linda/*\npath-exclude=/usr/share/locale/*' > /etc/dpkg/dpkg.cfg.d/01_nodoc &&\
    echo 'Dir::Cache "";\nDir::Cache::archives "";' > /etc/apt/apt.conf.d/02nocache && \
    apt-get install --no-install-recommends -yq \
    wget \
    texlive-base \
    texlive-latex-base \
    texlive-latex-extra \
    texlive-luatex \
    texlive-pictures \
    texlive-lang-english \
    texlive-lang-german \
    texlive-fonts-extra \
    texlive-science \
&& rm -rf /var/lib/apt/lists/*

RUN cd /tmp && \
    wget --no-check-certificate https://gitlab.ldv.ei.tum.de/no72gug/ldvklassen/-/archive/master/ldvklassen-master.tar.gz && \
    tar xvzf ldvklassen-master.tar.gz && \
    cd ldvklassen-master && \
    chmod +x install.sh && \
    ./install.sh && \
    rm -rf ldvklassen-master \
    
ENTRYPOINT ["lualatex"]
